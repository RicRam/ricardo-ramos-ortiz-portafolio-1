
#include <stdio.h>
int polinomio(int,int);

int main()
{
    int n,k;
    printf("Este program te muestra el proceso de resolucion de un polinomio (x+1)^n");
    printf("\nDigitar n:\n");
    scanf("%d", &n);
    for (k=n; k>=0; k--) {
        printf("(%d)^%d+ ",polinomio(n,k),n);

    }
}

int polinomio(int n,int k)
{
    if(k==0||k==n)
        return 1;
    return polinomio(n-1,k-1)+ polinomio(n-1,k);

}