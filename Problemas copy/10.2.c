#include <stdio.h>

int BubbleSort(int a[],int n);
int mediana(int a[],int n);

int main(int argc, const char * argv[])
{

    int n,i;
    printf("\t\t******Este programa te imprime la mediana de una lista.******\n");
    printf("Introduzca el tamaño de la lista:\n");
    scanf("%d", &n);
    int x[n];
    for (i=0; i<n; i++) {
        scanf("%d", &x[i]);
    }
    BubbleSort(x,n);
    printf("La lista ordenada es:\n");
    for (i=0; i<n; i++) {
        printf("%d ",x[i]);
    }
    mediana(x,n);
    printf("\nLa mediana de la lista es %d.",mediana(x,n));
    
}


int BubbleSort(int a[],int n)
{
    int i,j;
    int bubble;
    
    for (i=0; i<n; i++) {
        for (j=0; j<n; j++)
        {
            if (a[j]>a[j+1]) {
                bubble=a[j];
                a[j]=a[j+1];
                a[j+1]=bubble;
            }
        }
    }
    
    for (i=0; i<n; i++) {
        return a[i];
    }

}

int mediana(int a[],int n)
{
    int limI,limS,pivote;
    limI=0;
    limS=n;
    pivote=(limI+limS)/2;
    return a[pivote];
}