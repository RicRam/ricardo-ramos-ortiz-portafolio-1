#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void burbuja(int [],int n);

void main( )
{
    int num[999],n,i;
    int hora = time(NULL);
    printf("\t\tEste programa te escribe una lista de 999 numeros aleatorios del 0 al 2000:\n\n\n");
    srand(hora);
    for(i = 0; i<999;i++)
    {
        num[i] = rand()%2001;
    }
    burbuja(num, 999);
}

void burbuja(int a[],int n)
{
    int i,j;
    int bubble;

    for (i=0; i<n; i++) {
        for (j=0; j<n; j++)
        {
            if (a[j]>a[j+1]) {
                bubble=a[j];
                a[j]=a[j+1];
                a[j+1]=bubble;
            }
        }
    }
    
    for (i=0; i<n; i++) {
        printf("%d ", a[i]);
    }
}