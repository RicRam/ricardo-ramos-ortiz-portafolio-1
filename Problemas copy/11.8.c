#include <stdio.h>


typedef struct
{
    long int a,b,c;
} Area;

int main()
{
    Area x,y,z;
    printf("Este programa te permite introducir los lados de un triangulo para calcular el area.\n");
    printf("Digete primer lado\n");
    scanf("%ld", &x.a);
    printf("Digite segundo lado\n");
    scanf("%ld", &y.b);
    printf("Digite tercer lado\n");
    scanf("%ld", &z.c);

    long int p,A;
    p=(x.a+y.b+z.c)/2;
    A=p*((p-x.a)*(p-y.b)*(p-z.c));
    
    if (A<0){
        A*=-1;
    }
    if (A>0){
        A=A;}
    
    double r=A, i=0;
    while (i!=r){
        i=r;
        r=(A/r+r)/2;
    }
    printf("El area es %.2lf",r);
    
}


