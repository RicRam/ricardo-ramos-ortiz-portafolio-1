#include <stdio.h>

int main()
{
    int lista[]={1,2,3,4,5,6,7};
    int i,k=6,burbuja=0;
    
    printf("Este programa te ordenara la siguiente lista: ");
    for (i=0; i<7; i++) {
        printf("%d,", lista[i]);
    }
    printf(" de mayor a menor:\n\n");
    
    for(i=7;i>0;i--,k--)
    {
        burbuja=lista[i];
        lista[i]=lista[k];
         lista[k]=burbuja;
        printf("%d ", lista[i]);
        
    }
}
