#include <stdio.h>
#include <stdlib.h>
long factorial(long n);
int main ()
{
    long n;
    scanf("%ld", &n);
    printf("%ld", factorial(n));
    
}
long factorial(long n)
{
    if (n==0 || n==1)
        return 1;
    else
        return n*factorial(--n); // El error de este codigo radica en la ultima operacion de 'n' donde se debe escribir 'n-1' para que logre llegar a modificarse bien, ya que si se deja como esta '--n' el valor de esa variable se modificara antes de lo debido.
}