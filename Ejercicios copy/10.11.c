
#include <stdio.h>
#include <stdlib.h>

int binario(int a[],int num,int limI,int limS);

int main()
{
    int a[]={10,9,8,7,6,5,4,3,2,1,0},num;
       printf("Digite numero a buscar: ");
    scanf("%d", &num);
    printf("\nEsta en la posicion: %d del arreglo", binario(a,num,0,11));
    
}

int binario(int a[],int num,int limI,int limS)
{
        int pivote=0;
    if (limI>limS)
        return -1;
    pivote=(limI+limS)/2;
    if (num==a[pivote])
        return pivote;
    
    else if (num<a[pivote])
        return binario(a, num, pivote+1, limS);
    else if (num>a[pivote])
        return binario(a, num, limI, pivote-1);
}