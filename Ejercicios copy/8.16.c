#include <stdio.h>

long long potencia(int n, int x);

int main()
{
    int n, x;
       printf("Digite el valor de base:\n");
    scanf("%d", &x);
    printf("Digite el valor de exponente: \n");
    scanf("%d", &n);

      potencia(n,x);
    printf("%lld", potencia(n, x));
  
}

long long potencia(int n, int x)
{
    if (n==0)
        return 1;
    
    return x*(potencia(n-1, x));
}

