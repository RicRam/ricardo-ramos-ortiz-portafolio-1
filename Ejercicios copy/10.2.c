#include <stdio.h>

void Eliminar(int a[], int n);

int main()
{
    int n,i;
    printf("Determine el tamaño de la lista:\n");
    scanf("%d", &n);
    int array[n];
    printf("Escribir lista:\n");
    for (i=0; i<n; i++) {
        scanf("%d",&array[i]);
    }
    
    Eliminar(array,n);
    
}

void Eliminar(int a[], int n)
{
    int i,j,k,arr[n];
    
    for (i=0; i<n; i++)
    {
        j=a[i];
        for (k=i+1; k<=n; k++) {
            if (a[k]==j) {
                a[k]=0;
            }
        }
    }
    for (i=0; i<n; i++) {
        printf("%d ", a[i]);
    }
}