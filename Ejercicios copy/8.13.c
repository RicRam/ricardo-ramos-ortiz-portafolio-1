#include <stdio.h>

void hanoi(char vari,char varc,char varf, int n);

int main()
{
    int n;
    char vari, varc,varf;

    scanf("%d", &n);
    vari='A';
    varc='B';
    varf='C';
    hanoi(vari, varc, varf, n);
    
}
void hanoi(char vari,char varc,char varf, int n)
{
    if(n==1)
        printf("Mover disco %d desde varilla %c a varilla %c\n",n,vari,varf);
    else
    {
        hanoi(vari,varf,varc,n-1);
        printf("Mover disco %d desde varilla %c a varilla %c\n",n,vari,varf);
        hanoi(varc,vari,varf,n-1);
    }
}