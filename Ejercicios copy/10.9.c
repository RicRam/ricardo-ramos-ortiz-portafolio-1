#include <stdio.h>

int binario(int a[],int n,int limI,int limS);

int main()
{
    int a[]={8,13,17,26,44,56,88,97};

    printf("%d", binario(a,88,0,8));
    
}

int binario(int a[],int n,int limI,int limS)
{
    int pivote=0;
    if (limI>limS) {
        return -1;
    }
    pivote=(limI+limS)/2;
    
    if (n==a[pivote]) {
        return pivote;
    }
    else if (n>a[pivote])
        return binario(a,n,pivote+1, limS);
    else if (n<a[pivote])
        return binario(a,88, limI, pivote-1);
}/* TRAZAMIENTO:

Primero se mide el arreglo para saber cuales son los dos limites (INFERIOR y SUPERIOI) despues, se indentifica el medio del arreglo y se compara para ver si el 88 esta en el medio del arreglo, al ver que el 88 es mayor se sustituye el inferior por la mitad, volviendo ha repetir el proceso hasta encontrar el 88*/